﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.UI.Options;
using CombatHandler.Generic;
using System.Collections.Generic;
using System.Linq;

namespace Desu
{
    class DocCombatHandler : GenericCombatHandler
    {
        private Menu _menu;

        private List<PerkHash> BattleGroupHeals = new List<PerkHash> {
            PerkHash.BattlegroupHeal1,
            PerkHash.BattlegroupHeal2,
            PerkHash.BattlegroupHeal3,
            PerkHash.BattlegroupHeal4,
        };
        public DocCombatHandler()
        {

            //LE Proc
            RegisterPerkProcessor(PerkHash.LEProcDoctorAntiseptic, LEProcPerks);
            RegisterPerkProcessor(PerkHash.LEProcDoctorHealingCare, LEProcPerks);

            //Perks
            RegisterPerkProcessor(PerkHash.TaintWounds, DamagePerk);
            RegisterPerkProcessor(PerkHash.ChemicalBlindness, DamagePerk);
            RegisterPerkProcessor(PerkHash.PoisonSprinkle, DamagePerk);

            RegisterPerkProcessor(PerkHash.QuickShot, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.DoubleShot, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.Deadeye, TargetedDamagePerk);

            RegisterPerkProcessor(PerkHash.Energize, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.PowerVolley, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.PowerShock, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.PowerBlast, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.PowerCombo, TargetedDamagePerk);

            RegisterPerkProcessor(PerkHash.HostileTakeover, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.ChaoticAssumption, TargetedDamagePerk);

            RegisterPerkProcessor(PerkHash.DazzleWithLights, StarfallPerk);
            RegisterPerkProcessor(PerkHash.Combust, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.ThermalDetonation, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.Supernova, TargetedDamagePerk);

            BattleGroupHeals.ForEach(p => RegisterPerkProcessor(p, MajorHealPerk));

            //Spells
            RegisterSpellProcessor(RelevantNanos.IMPROVED_CH, ICHSingleTargetHeal, CombatActionPriority.High);
            RegisterSpellProcessor(RelevantNanos.BODILY_INV, SingleTargetHeal, CombatActionPriority.Medium);
            RegisterSpellProcessor(RelevantNanos.SUPHEALTHPLAN, TeamHeal, CombatActionPriority.Low);

            RegisterSpellProcessor(RelevantNanos.HP_BUFFS, TeamHPBuff);

            if (HasNano(RelevantNanos.IMPROVED_LC))
            {
                RegisterSpellProcessor(RelevantNanos.IMPROVED_LC, TeamShortHPBuff);
            }
            else
            {
                RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.DoctorShortHPBuffs).OrderByStackingOrder(), TeamShortHPBuff);
            }


            RegisterSpellProcessor(RelevantNanos.CompositeAttribute, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeNano, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeUtility, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeRanged, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeRangedSpec, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.ImpInstinctControl, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.ImpNanoRepulsor, GenericBuff);
            //RegisterSpellProcessor(RelevantNanos.OMNI_MED, GenericBuff);
            //RegisterSpellProcessor(RelevantNanos.ImpLifeChanneler, GenericBuff);

            //This needs work 
            RegisterSpellProcessor(RelevantNanos.UBT, DebuffTarget);

            _menu = new Menu("CombatHandler.Doc", "CombatHandler.Doc");
            _menu.AddItem(new MenuBool("UseDebuff", "Doc Debuffing", true));
            _menu.AddItem(new MenuBool("LEProcPerks", "Use LE Procs", true));
            _menu.AddItem(new MenuBool("UseHPBuff", "Allow for Long HP Buffs", true));
            _menu.AddItem(new MenuBool("UseShortHPBuff", "Allow for Short HP Buffs", true));
            _menu.AddItem(new MenuBool("AllowICH", "Allow for iCH - Missing 35k", true));
            OptionPanel.AddMenu(_menu);
        }

        protected bool LEProcPerks(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("LEProcPerks"))
                return false;

            foreach (Buff buff in DynelManager.LocalPlayer.Buffs.AsEnumerable())
            {
                if (buff.Name == perk.Name)
                {
                    return false;
                }
            }
            return true;
        }


        private bool ICHSingleTargetHeal(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("AllowICH"))
                return false;

            // Prioritize keeping ourself alive
            if (DynelManager.LocalPlayer.HealthPercent <= 15)
            {
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            // Try to keep our teammates alive if we're in a team
            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.MissingHealth > 35000)
                    .FirstOrDefault();

                if (dyingTeamMember != null)
                {
                    actionTarget.Target = dyingTeamMember;
                    return true;
                }
            }

            return false;
        }



        private bool DebuffTarget(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            // Check if we are fighting and if debuffing is enabled
            if (fightingTarget == null || !_menu.GetBool("UseDebuff"))
                return false;

            //Check if the target has the ubt buff running
            foreach (Buff buff in fightingTarget.Buffs.AsEnumerable())
                if (buff.Name == spell.Name)
                    return false;

            //Check if you are low hp dont debuff
            if (DynelManager.LocalPlayer.HealthPercent <= 60)
            {
                //actionTarget.Target = DynelManager.LocalPlayer;
                return false;
            }

            //Check if we're in a team and someone is low hp , dont debuff
            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => c.IsAlive)
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.HealthPercent < 60)
                    .OrderByDescending(c => c.GetStat(Stat.NumFightingOpponents))
                    .FirstOrDefault();

                if (dyingTeamMember != null)
                {
                    actionTarget.Target = dyingTeamMember;
                    return false;
                }
            }
            return true;
        }

        private bool MajorHealPerk(PerkAction perkAction, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            // Prioritize keeping ourself alive
            if (DynelManager.LocalPlayer.HealthPercent <= 30)
            {
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            // Try to keep our teammates alive if we're in a team
            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.HealthPercent < 30)
                    .OrderByDescending(c => c.GetStat(Stat.NumFightingOpponents))
                    .FirstOrDefault();

                if (dyingTeamMember != null)
                {
                    actionTarget.Target = dyingTeamMember;
                    return true;
                }
            }

            return false;
        }

        protected virtual bool StarfallPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (PerkAction.Find(PerkHash.Combust, out PerkAction combust) && !combust.IsAvailable)
                return false;

            return TargetedDamagePerk(perkAction, fightingTarget, ref actionTarget);
        }

        private bool SingleTargetHeal(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            // Prioritize keeping ourself alive
            if (DynelManager.LocalPlayer.HealthPercent <= 50)
            {
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            // Try to keep our teammates alive if we're in a team
            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.HealthPercent <= 60 || c.MissingHealth >15000)
                    .OrderByDescending(c => c.GetStat(Stat.NumFightingOpponents))
                    .FirstOrDefault();

                if (dyingTeamMember != null)
                {
                    actionTarget.Target = dyingTeamMember;
                    return true;
                }
            }

            return false;
        }

        private bool TeamHeal(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            // Prioritize keeping ourself alive
            if (DynelManager.LocalPlayer.HealthPercent <= 90 && DynelManager.LocalPlayer.HealthPercent >= 60)
            {
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            // Try to keep our teammates alive if we're in a team
            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.HealthPercent >= 60)
                    .Where(c => c.HealthPercent <= 90)
                    .OrderByDescending(c => c.GetStat(Stat.NumFightingOpponents))
                    .FirstOrDefault();

                if (dyingTeamMember != null)
                {
                    actionTarget.Target = dyingTeamMember;
                    return true;
                }
            }

            return false;
        }

        private bool TeamHPBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("UseHPBuff") || HasBuffNanoLine(NanoLine.DoctorHPBuffs, DynelManager.LocalPlayer))
            {
                return false;
            }
            return GenericBuff(spell, fightingTarget, ref actionTarget);
        }

        private bool TeamShortHPBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("UseShortHPBuff") || HasBuffNanoLine(NanoLine.DoctorShortHPBuffs, DynelManager.LocalPlayer))
            {
                return false;
            }

            return GenericBuff(spell, fightingTarget, ref actionTarget);
        }


            private bool HasNano(int nanoId)
        {
            return Spell.Find(nanoId, out Spell spell);
        }


        private static class RelevantNanos
        {
            public const int CompositeAttribute = 223372;
            public const int CompositeNano = 223380;
            public const int CompositeUtility = 287046;
            public const int CompositeRanged = 223348;
            public const int CompositeRangedSpec = 223364;
            public const int ImpInstinctControl = 222856;
            public const int ImpNanoRepulsor = 222823;
            public const int ContReconstruction = 222824;
            public const int IMPROVED_LC = 275011;
            public static int[] HP_BUFFS = new[] { 95709, 28662, 95720, 95712, 95710, 95711, 28649, 95713, 28660, 95715, 95714, 95718, 95716, 95717, 95719, 42397 };

            public const int SUPHEALTHPLAN = 273312;
            public const int IMPROVED_CH = 270747;
            public const int BODILY_INV = 223299;
            public const int UBT = 99577;
            public const int UBT_MONSTER = 301844;
            public const int UBT_HUMAN = 301843;
        }
    }
}