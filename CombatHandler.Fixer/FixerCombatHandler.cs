﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI.Options;
using System.Collections.Generic;
using CombatHandler.Generic;
using System.Linq;
using System;

namespace Desu
{
    
    public class FixerCombathandler : GenericCombatHandler
    {
        private Menu _menu;

        private double _lastBackArmorCheckTime = Time.NormalTime;
        public FixerCombathandler()
        {
            //LE Proc
            RegisterPerkProcessor(PerkHash.LEProcFixerFishInABarrel, LEProcPerks);  //Type 1
            RegisterPerkProcessor(PerkHash.LEProcFixerSlipThemAMickey, LEProcPerks);     //Type 2

            //DmgPerks
            RegisterPerkProcessor(PerkHash.Energize, DmgBuffPerk);
            RegisterPerkProcessor(PerkHash.ReinforceSlugs, DmgBuffPerk);

            // Dark Kin Line
            RegisterPerkProcessor(PerkHash.PowerBolt, DamagePerk);
            RegisterPerkProcessor(PerkHash.Numb, DamagePerk);
            RegisterPerkProcessor(PerkHash.Cripple, DamagePerk);

            // Grid NCU Extentions
            RegisterPerkProcessor(PerkHash.NCUBooster, DamagePerk);

            // Hit and Run Line
            RegisterPerkProcessor(PerkHash.TriggerHappy, DamagePerk);
            RegisterPerkProcessor(PerkHash.EatBullets, DamagePerk);

            // Thief Line -- Removed; Hard to use as the target must be behind the Fixer
            //RegisterPerkProcessor(PerkHash.Escape, DamagePerk);

            // Illogical Patterns Line
            RegisterPerkProcessor(PerkHash.Guesstimate, DamagePerk);
            RegisterPerkProcessor(PerkHash.MemoryScrabble, DamagePerk);

            // Mountaineer Line
            RegisterPerkProcessor(PerkHash.EncaseInStone, DamagePerk);
            RegisterPerkProcessor(PerkHash.DetonateStoneworks, DamagePerk);

            // Power Up Line
            RegisterPerkProcessor(PerkHash.Energize, DamagePerk);
            RegisterPerkProcessor(PerkHash.PowerVolley, DamagePerk);
            RegisterPerkProcessor(PerkHash.PowerShock, DamagePerk);
            RegisterPerkProcessor(PerkHash.PowerBlast, DamagePerk);
            RegisterPerkProcessor(PerkHash.PowerCombo, DamagePerk);

            // SMG Mastery            
            RegisterPerkProcessor(PerkHash.ReinforceSlugs, DamagePerk);
            RegisterPerkProcessor(PerkHash.JarringBurst, DamagePerk);
            RegisterPerkProcessor(PerkHash.SolidSlug, DamagePerk);
            RegisterPerkProcessor(PerkHash.NeutroniumSlug, DamagePerk);

            // The Unknown Factor Line
            RegisterPerkProcessor(PerkHash.HostileTakeover, DamagePerk);
            RegisterPerkProcessor(PerkHash.ChaoticAssumption, DamagePerk);

            //Evasive Perks
            RegisterPerkProcessor(PerkHash.EvasiveStance, EvasivePerk);

            //Buffs
            RegisterSpellProcessor(RelevantNanos.CompositeAttribute, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeNano, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeRanged, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeRangedSpec, GenericBuff);
            //RegisterSpellProcessor(RelevantNanos.IFrenzyofShells, GenericBuff);
            //RegisterSpellProcessor(RelevantNanos.BlessedbyShadow, GenericBuff);
            //RegisterSpellProcessor(RelevantNanos.LastingUltimatum, GenericBuff);
            //RegisterSpellProcessor(RelevantNanos.SuperiorInsuranceHack, GenericBuff);
            //RegisterSpellProcessor(RelevantNanos.ISemiSentientAugCloud, GenericBuff);
            //RegisterSpellProcessor(RelevantNanos.SentientViralRecoder, GenericBuff);
            //RegisterSpellProcessor(RelevantNanos.FirewalledSyncCompressor, GenericBuff);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.DamageBuffs_LineA).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.FixerDodgeBuffLine).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.FixerSuppressorBuff).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(RelevantNanos.NCU_BUFFS, NCUBuff);
            RegisterSpellProcessor(RelevantNanos.GREATER_PRESERVATION_MATRIX, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.TEAM_LONG_HOTS, LongHotBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.HealOverTime).OrderByStackingOrder(), ShortHotBuff);
            RegisterSpellProcessor(RelevantNanos.RK_RUN_BUFFS, GsfBuff);
            RegisterSpellProcessor(RelevantNanos.SL_RUN_BUFFS, ShadowlandsSpeedBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.EvasionDebuffs).OrderByStackingOrder(), EvasionDebuff);


            //Items
            RegisterItemProcessor(RelevantItems.DreadlochEnduranceBoosterNanomageEdition, RelevantItems.DreadlochEnduranceBoosterNanomageEdition, EnduranceBooster, CombatActionPriority.High);

            _menu = new Menu("CombatHandler.Fixer", "CombatHandler.Fixer");
            _menu.AddItem(new MenuBool("UseRKRunspeed", "Use RK Runspeed Buffs", false));
            _menu.AddItem(new MenuBool("UseEvasionDebuff", "Use Evasion Debuffs", false));
            _menu.AddItem(new MenuBool("UseLongHoT", "Use Long-term HoTs", true));
            _menu.AddItem(new MenuBool("UseShortHoT", "Use Short-term HoTs", true));
            _menu.AddItem(new MenuBool("LEProcPerks", "Use LE Procs", true));
            OptionPanel.AddMenu(_menu);
        }

        protected bool LEProcPerks(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("LEProcPerks"))
                return false;

            foreach (Buff buff in DynelManager.LocalPlayer.Buffs.AsEnumerable())
            {
                if (buff.Name == perk.Name)
                {
                    return false;
                }
            }
            return true;
        }

        protected bool ToggledTeamBuff(string settingName, Spell spell, SimpleChar fightingTarget, Func<SimpleChar, bool> hasBuffCheck, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool(settingName))
            {
                return false;
            }
            return TeamBuff(spell, fightingTarget, hasBuffCheck, ref actionTarget);
        }

        protected bool TeamBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return TeamBuff(spell, fightingTarget, c => HasBuff(spell, c), ref actionTarget);
        }

        protected bool TeamBuff(Spell spell, SimpleChar fightingTarget, Func<SimpleChar, bool> hasBuffCheck, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget != null || !CanCast(spell))
            {
                return false;
            }

            if (!hasBuffCheck.Invoke(DynelManager.LocalPlayer))
            {
                actionTarget.Target = DynelManager.LocalPlayer;
                actionTarget.ShouldSetTarget = true;
                return true;
            }

            return false;
        }

        protected bool ShouldRefreshBuff(Spell spell, Buff buff)
        {
            if (buff.Nanoline != spell.Nanoline)
            {
                return false;
            }

            return buff.StackingOrder > spell.StackingOrder || buff.StackingOrder == spell.StackingOrder && buff.RemainingTime / buff.TotalTime > 0.1;
        }

        protected bool CanCast(Spell spell)
        {
            return spell.Cost < DynelManager.LocalPlayer.Nano;
        }

        protected bool HasBuff(Spell spell, SimpleChar target)
        {
            return target.Buffs.Any(buff => ShouldRefreshBuff(spell, buff));
        }

        private bool DmgBuffPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!DynelManager.LocalPlayer.IsAttacking || fightingTarget == null)
                return false;
            return true;
        }

        private bool EvasivePerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.HealthPercent <= 50)
                return true;

            return false;
        }

        private bool NCUBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (HasBuffNanoLine(NanoLine.FixerNCUBuff, DynelManager.LocalPlayer))
            {
                return false;
            }
            return GenericBuff(spell, fightingTarget, ref actionTarget);
        }

        private bool LongHotBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return ToggledTeamBuff("UseLongHoT", spell, fightingTarget, target => HasBuffNanoLine(NanoLine.FixerLongHoT, target), ref actionTarget);
        }

        private bool ShortHotBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return HealOverTimeTeamBuff("UseShortHoT", spell, fightingTarget, ref actionTarget);
        }

        private bool EvasionDebuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return ToggledDebuffTarget("UseEvasionDebuff", spell, fightingTarget, ref actionTarget);
        }

        protected bool HealOverTimeTeamBuff(string toggleName, Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return ToggledTeamBuff(toggleName, spell, fightingTarget, target => HasBuffNanoLine(NanoLine.HealOverTime, target) || HasBuffNanoLine(NanoLine.MongoBuff, target), ref actionTarget);
        }

        private bool GsfBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("UseRKRunspeed"))
            {
                return false;
            }

            if (HasBuffNanoLine(NanoLine.RunspeedBuffs, DynelManager.LocalPlayer))
            {
                return false;
            }

            return GenericBuff(spell, fightingTarget, ref actionTarget); 

        }

        private bool ShadowlandsSpeedBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_menu.GetBool("UseRKRunspeed"))
            {
                return false;
            }

            return GenericBuff(spell, fightingTarget, ref actionTarget);
        }

        protected bool ToggledDebuffTarget(string settingName, Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            // Check if we are fighting and if debuffing is enabled
            if (fightingTarget == null || !_menu.GetBool(settingName))
            {
                return false;
            }

            return !fightingTarget.Buffs.Any(buff => ShouldRefreshBuff(spell, buff));
        }

        protected override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);

            CancelBuffs(_menu.GetBool("UseRKRunspeed") ? RelevantNanos.SL_RUN_BUFFS : RelevantNanos.RK_RUN_BUFFS);

        }



         private bool EnduranceBooster(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            // don't use if skill is locked (we will add this dynamically later)
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Strength))
                return false;

            // don't use if we're above 40%
            if (DynelManager.LocalPlayer.HealthPercent > 40)
                return false;

            // don't use if nothing is fighting us
            //if (DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) == 0)
            //    return false;

            // don't use if we have another major absorb running
            // we could check remaining absorb stat to be slightly more effective
            if (DynelManager.LocalPlayer.Buffs.Contains(NanoLine.BioCocoon))
                return false;

            return true;
        }

        private static class RelevantNanos
        {
            public const int CompositeAttribute = 223372;
            public const int CompositeNano = 223380;
            public const int CompositeRanged = 223348;
            public const int CompositeRangedSpec = 223364;

            public const int GREATER_PRESERVATION_MATRIX = 275679;
            public static readonly int[] SL_RUN_BUFFS = { 223125, 223131, 223129, 215718, 223127, 272416, 272415, 272414, 272413, 272412 };
            public static readonly int[] RK_RUN_BUFFS = { 93132, 93126, 93127, 93128, 93129, 93130, 93131, 93125 };
            public static readonly int[] SUMMON_GRID_ARMOR = { 155189, 155187, 155188, 155186 };
            public static readonly int[] SUMMON_SHADOWWEB_SPINNER = { 273349, 224422, 224420, 224418, 224416, 224414, 224412, 224410, 224408, 224405, 224403 };
            public static readonly int[] NCU_BUFFS = { 275043, 163095, 163094, 163087, 163085, 163083, 163081, 163079, 162995 };
            public static readonly Spell[] TEAM_LONG_HOTS = Spell.GetSpellsForNanoline(NanoLine.FixerLongHoT).OrderByStackingOrder().Where(spell => spell.Identity.Instance != GREATER_PRESERVATION_MATRIX).ToArray();

            /*
            public const int IFrenzyofShells = 273355;
            public const int BlessedbyShadow = 223125;
            public const int LastingUltimatum = 252050;
            public const int SuperiorInsuranceHack = 273352;
            public const int ISemiSentientAugCloud = 222838;
            public const int SentientViralRecoder = 163095;
            public const int FirewalledSyncCompressor = 275043;
            */
        }

        private static class RelevantItems
        {
            public const int DreadlochEnduranceBooster = 267168;
            public const int DreadlochEnduranceBoosterNanomageEdition = 267167;
            public static readonly int[] GRID_ARMORS = { 155172, 155173, 155174, 155150 };
            public static readonly int[] SHADOWWEB_SPINNERS = { 273350, 224400, 224399, 224398, 224397, 224396, 224395, 224394, 224393, 224392, 224390 };

        }
        private enum BackItemType
        {
            SHADOWWEB_SPINNER = 0,
            GRID_ARMOR = 1
        }

    }
}
