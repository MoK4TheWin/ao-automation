﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.UI.Options;
using CombatHandler.Generic;
using System.Collections.Generic;
using System.Linq;

namespace Desu
{
    class AdvCombatHandler : GenericCombatHandler
    {
        private Menu _menu;

        public AdvCombatHandler()
        {
            //LE Proc
            RegisterPerkProcessor(PerkHash.LEProcAdventurerMacheteFlurry, LEProcPerks);  //Type 1
            RegisterPerkProcessor(PerkHash.LEProcAdventurerCombustion, LEProcPerks);     //Type 2

            //BioShielding Perkline
            RegisterPerkProcessor(PerkHash.BioRejuvenation, TeamHealPerk);
            RegisterPerkProcessor(PerkHash.BioRegrowth, TeamHealPerk);
            RegisterPerkProcessor(PerkHash.BioShield, SelfHealPerk);
            RegisterPerkProcessor(PerkHash.BioCocoon, SelfHealPerk);

            RegisterPerkProcessor(PerkHash.Devour, DamagePerk);
            RegisterPerkProcessor(PerkHash.BleedingWounds, DamagePerk);
            RegisterPerkProcessor(PerkHash.GuttingBlow, DamagePerk);

            RegisterPerkProcessor(PerkHash.LightBullet, DamagePerk);
            RegisterPerkProcessor(PerkHash.PowerOfLight, DamagePerk);
            RegisterPerkProcessor(PerkHash.LightKiller, DamagePerk);

            RegisterPerkProcessor(PerkHash.Stoneworks, DamagePerk);

            RegisterPerkProcessor(PerkHash.ShadowStab, DamagePerk);
            RegisterPerkProcessor(PerkHash.BladeOfNight, DamagePerk);
            RegisterPerkProcessor(PerkHash.ShadowKiller, DamagePerk);

            RegisterPerkProcessor(PerkHash.Beckoning, DamagePerk);
            RegisterPerkProcessor(PerkHash.NocturnalStrike, DamagePerk);
            RegisterPerkProcessor(PerkHash.Awakening, DamagePerk);

            RegisterPerkProcessor(PerkHash.FullFrontal, DamagePerk);
            RegisterPerkProcessor(PerkHash.Confinement, DamagePerk);

            RegisterPerkProcessor(PerkHash.QuickCut, DamagePerk);
            RegisterPerkProcessor(PerkHash.Flay, DamagePerk);
            RegisterPerkProcessor(PerkHash.FlurryOfCuts, DamagePerk);
            RegisterPerkProcessor(PerkHash.RibbonFlesh, DamagePerk);

            RegisterPerkProcessor(PerkHash.EncaseInStone, DamagePerk);
            RegisterPerkProcessor(PerkHash.DetonateStoneworks, DamagePerk);

            RegisterPerkProcessor(PerkHash.QuickShot, DamagePerk);
            RegisterPerkProcessor(PerkHash.DoubleShot, DamagePerk);
            RegisterPerkProcessor(PerkHash.Deadeye, DamagePerk);

            RegisterPerkProcessor(PerkHash.EncaseInStone, DamagePerk);

            //Buffs
            RegisterSpellProcessor(RelevantNanos.CompositeAttribute, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeNano, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeUtility, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeRanged, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeRangedSpec, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.ImprovedVengeanceofNature, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CyclicRetributionoftheAesir, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.RetributionoftheAesir, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.DanceoftheDervish, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.TheGreatExplorer, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.BoonoftheWanderer, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.BalladofthePlainsWanderer, GenericBuff);
            //RegisterSpellProcessor(RelevantNanos.TeamFreeMovement, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.FreedomoftheWanderer, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.Backstabber, GenericBuff);

            //Calia's Form (Sabre Team)
            RegisterPerkProcessor(PerkHash.Beckoning, CerberusPerk);
            RegisterSpellProcessor(RelevantNanos.FormofCerberus, Cerberus);
            //RegisterSpellProcessor(RelevantNanos.CaliaAnatomySabretooth, CaliasForm); //Cannot use Sabre Team in this form
            //RegisterSpellProcessor(RelevantNanos.CaliaSabretoothTeam, CaliasForm);
            //RegisterSpellProcessor(RelevantNanos.PoisonousBite, CaliasForm); //Currently not working

            //Spells
            RegisterSpellProcessor(RelevantNanos.BeautyofLife, SingleTargetHeal);
            RegisterSpellProcessor(RelevantNanos.SuperiorSeedLife, TeamHeal, CombatActionPriority.High);


            _menu = new Menu("CombatHandler.Adv", "CombatHandler.Adv");
            _menu.AddItem(new MenuBool("UseCerberus", "Include Form of Cerberus?", false));
            //_menu.AddItem(new MenuBool("UseSabretooth", "Include Calia's Form: Sabretooth (Team)?", false));
            _menu.AddItem(new MenuBool("LEProcPerks", "Use LE Procs", true));
            OptionPanel.AddMenu(_menu);
        }

        protected bool LEProcPerks(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("LEProcPerks"))
                return false;

            foreach (Buff buff in DynelManager.LocalPlayer.Buffs.AsEnumerable())
            {
                if (buff.Name == perk.Name)
                {
                    return false;
                }
            }
            return true;
        }

        protected bool Cerberus(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("UseCerberus"))
                return false;

            if (fightingTarget != null)
                return false;

            if (DynelManager.LocalPlayer.Buffs.Find(spell.Nanoline, out Buff buff))
            {
                //Don't cast if weaker than existing
                if (spell.StackingOrder < buff.StackingOrder)
                    return false;

                //Don't cast if greater than 10% time remaining
                if (spell.Nanoline == buff.Nanoline && buff.RemainingTime / buff.TotalTime > 0.1)
                    return false;
            }
            else
            {
                if (DynelManager.LocalPlayer.RemainingNCU < spell.NCU)
                    return false;
            }

            actionTarget.ShouldSetTarget = true;
            return true;
        }



        protected virtual bool CerberusPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("UseCerberus") || !DynelManager.LocalPlayer.IsAttacking || fightingTarget == null || fightingTarget.Health < 50000 || fightingTarget.HealthPercent < 5)
                return false;

            return true;
        }

        private bool CaliasForm(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("UseSabretooth") || DynelManager.LocalPlayer.Nano < spell.Cost)
                return false;

            return true;
        }
        private bool TeamHealPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {

            if (!DynelManager.LocalPlayer.IsAttacking)
                return false;

            // Prioritize keeping ourself alive
            if (DynelManager.LocalPlayer.HealthPercent <= 60)
            {
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            // Try to keep our teammates alive if we're in a team
            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => c.IsAlive)
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.HealthPercent <= 60)
                    .OrderByDescending(c => c.GetStat(Stat.NumFightingOpponents))
                    .FirstOrDefault();

                if (dyingTeamMember != null)
                {
                    actionTarget.Target = dyingTeamMember;
                    return true;
                }
            }
            return false;
        }

        private bool SelfHealPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!DynelManager.LocalPlayer.IsAttacking)
                return false;

            // Prioritize keeping ourself alive
            if (DynelManager.LocalPlayer.HealthPercent <= 35)//We should consider making this a slider value in the options
            {
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }
            return false;
        }

        private bool MajorHealPerk(PerkAction perkAction, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            // Prioritize keeping ourself alive
            if (DynelManager.LocalPlayer.HealthPercent <= 50)
            {
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            // Try to keep our teammates alive if we're in a team
            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.HealthPercent < 50)
                    .OrderByDescending(c => c.GetStat(Stat.NumFightingOpponents))
                    .FirstOrDefault();

                if (dyingTeamMember != null)
                {
                    actionTarget.Target = dyingTeamMember;
                    return true;
                }
            }

            return false;
        }


        private bool TeamHeal(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            // Prioritize keeping ourself alive
            if (DynelManager.LocalPlayer.HealthPercent <= 90)
            {
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            // Try to keep our teammates alive if we're in a team
            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.HealthPercent <= 90)
                    .OrderByDescending(c => c.GetStat(Stat.NumFightingOpponents))
                    .FirstOrDefault();

                if (dyingTeamMember != null)
                {
                    actionTarget.Target = dyingTeamMember;
                    return true;
                }
            }

            return false;
        }
        private bool SingleTargetHeal(Spell spell, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            actiontarget.Target = DynelManager.LocalPlayer;
            return DynelManager.LocalPlayer.MissingHealth > 3000;
        }
        private static class RelevantNanos
        {
            public const int CompositeAttribute = 223372;
            public const int CompositeNano = 223380;
            public const int CompositeUtility = 287046;
            public const int CompositeRanged = 223348;
            public const int CompositeRangedSpec = 223364;

            public const int PoisonousBite = 273288;
            public const int ImprovedVengeanceofNature = 273290;
            public const int CyclicRetributionoftheAesir = 161571;
            public const int RetributionoftheAesir = 55837;
            public const int DanceoftheDervish = 161167;
            public const int TheGreatExplorer = 268027;
            public const int BoonoftheWanderer = 204318;
            public const int BalladofthePlainsWanderer = 161159;
            public const int TeamFreeMovement = 26698;
            public const int FreedomoftheWanderer = 204312;
            public const int Backstabber = 210797;

            public const int FormofCerberus = 275005;
            public const int CaliaAnatomySabretooth = 217678;
            public const int CaliaSabretoothTeam = 85090;

            public const int BeautyofLife = 223167;
            public const int SuperiorSeedLife = 270770;

        }
    }
}