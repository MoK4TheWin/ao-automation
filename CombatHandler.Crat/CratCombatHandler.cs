﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using AOSharp.Core.UI.Options;
using CombatHandler.Generic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Desu
{
    public class CratCombatHandler : GenericCombatHandler
    {
        private const float PostZonePetCheckBuffer = 5;
        private Menu _menu;
        private double _lastZonedTime = 0;

        // Dictatorship Perk Line
        private List<PerkHash> Dictatorship = new List<PerkHash>
            {
                PerkHash.Stab,
                PerkHash.Leadership,
                PerkHash.Governance,
                PerkHash.TheDirector
            };

        public CratCombatHandler()
        {
            //LE Procs
            RegisterPerkProcessor(PerkHash.LEProcBureaucratFormsInTriplicate, LEProcPerks);  //Type 1
            RegisterPerkProcessor(PerkHash.LEProcBureaucratTaxAudit, LEProcPerks);           //Type 2

            //Pet Spawners
            RegisterSpellProcessor(RelevantNanos.Pets.Where(x => x.Value.PetType == PetType.Attack).Select(x => x.Key).ToArray(), PetSpawner);
            RegisterSpellProcessor(RelevantNanos.Pets.Where(x => x.Value.PetType == PetType.Support).Select(x => x.Key).ToArray(), PetSpawner);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.CriticalDecreaseBuff).OrderByStackingOrder(), PetTargetBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.PetDefensiveNanos).OrderByStackingOrder(), PetTargetBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.PetShortTermDamageBuffs).OrderByStackingOrder(), PetTargetBuff);
            //RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.MPPetInitiativeBuffs).OrderByStackingOrder(), PetTargetBuff);
            //RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.MPPetDamageBuffs).OrderByStackingOrder(), PetTargetBuff);
            //RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.PetHealDelta843).OrderByStackingOrder(), PetTargetBuff);
            //RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.PetHealDelta850).OrderByStackingOrder(), PetTargetBuff);


            //Perks
            RegisterPerkProcessor(PerkHash.DazzleWithLights, StarfallPerk);
            RegisterPerkProcessor(PerkHash.Combust, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.ThermalDetonation, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.Supernova, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.QuickShot, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.DoubleShot, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.Deadeye, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.Antitrust, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.NanoFeast, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.BotConfinement, TargetedDamagePerk);

            //Buffs
            RegisterSpellProcessor(RelevantNanos.CompositeAttribute, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeNano, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeUtility, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeRanged, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeRangedSpec, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.ICutRedTape, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.IHeroicMeasures, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.DemSpeechDeadMan, GenericBuffDemSpeech);

            // Dictatorship Perk Line
            //Dictatorship.ForEach(p => RegisterPerkProcessor(p, DictatorshipPerk));


            //Spells
            //RegisterSpellProcessor(RelevantNanos.WorkplaceDepression, CratNuke); //Disabled
            RegisterSpellProcessor(RelevantNanos.MalaiseOfZeal, CratDebuff, CombatActionPriority.High);
            RegisterSpellProcessor(RelevantNanos.IntensifyStress, CratDebuff, CombatActionPriority.Medium);
            RegisterSpellProcessor(RelevantNanos.PinkSlip, CratNuke);
            RegisterSpellProcessor(RelevantNanos.WastefulArmMovements, CratDebuff, CombatActionPriority.High);
            RegisterSpellProcessor(RelevantNanos.TakeTheBullet, CastTakeTheBullet); 

            //Pet Buffs for CEO Guardian Only
            RegisterSpellProcessor(RelevantNanos.CorporateStrategy, PetCorporateStategy);
            RegisterSpellProcessor(RelevantNanos.GreaterCorporateInsurancePolicy, PetInsurancePolicy);
            RegisterSpellProcessor(RelevantNanos.DroidDamageMatrix, PetDroidMatrix);

            //Pet Shells
            foreach (int shellId in RelevantNanos.Pets.Values.Select(x => x.ShellId))
                RegisterItemProcessor(shellId, shellId, PetSpawnerItem);



            _menu = new Menu("CombatHandler.Crat", "CombatHandler.Crat");
            _menu.AddItem(new MenuBool("SpawnPets", "Spawn Pets?", true));
            _menu.AddItem(new MenuBool("BuffPets", "Buff Pets?", true));
            _menu.AddItem(new MenuBool("UseDebuff", "Crat Debuffing", true));
            _menu.AddItem(new MenuBool("UseNukes", "Crat Nuking", true));
            _menu.AddItem(new MenuBool("KillPet", "Use Take the Bullet", true));
            _menu.AddItem(new MenuBool("UseDemSpeech", "Use Dem Speech: Dead Man Walking", false));
            _menu.AddItem(new MenuBool("LEProcPerks", "Use LE Procs", true));

            OptionPanel.AddMenu(_menu);
        }

        protected bool LEProcPerks(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("LEProcPerks"))
                return false;

            foreach (Buff buff in DynelManager.LocalPlayer.Buffs.AsEnumerable())
            {
                if (buff.Name == perk.Name)
                {
                    return false;
                }
            }
            return true;
        }

        /*
        private bool DictatorshipPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget == null)
                return false;

            if (PerkAction.Find(PerkHash.Leadership, out PerkAction leadership))
                return true;

            if ((PerkAction.Find(PerkHash.Governance, out PerkAction governance)))
                return true;

            if (perkAction.Hash == PerkHash.Governance)
            {
                if (_actionQueue.Any(x => x.CombatAction is PerkAction action && (action == leadership)
                    || DynelManager.LocalPlayer.Buffs.Find(RelevantNanos.LeadershipBuff, out Buff buff1)
                    || DynelManager.LocalPlayer.Buffs.Find(RelevantNanos.TheDirectorBuff, out Buff buff2)))
                    return false;
            }

            if ((PerkAction.Find(PerkHash.TheDirector, out PerkAction thedirector)))
                return true;

            if (perkAction.Hash == PerkHash.TheDirector)
            {
                if (_actionQueue.Any(x => x.CombatAction is PerkAction action && (action == leadership || action == governance)
                    || DynelManager.LocalPlayer.Buffs.Find(RelevantNanos.LeadershipBuff, out Buff buff1)
                    || DynelManager.LocalPlayer.Buffs.Find(RelevantNanos.GovernanceBuff, out Buff buff2)))
                    return false;
            }

            return true;
        }
        */

        protected bool GenericBuffDemSpeech(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            // Check if we are fighting and if debuffing is enabled
            if (fightingTarget != null || !_menu.GetBool("UseDemSpeech"))
                return false;

            if (DynelManager.LocalPlayer.Buffs.Find(spell.Nanoline, out Buff buff))
            {
                //Don't cast if weaker than existing
                if (spell.StackingOrder < buff.StackingOrder)
                    return false;

                //Don't cast if greater than 10% time remaining
                if (spell.Nanoline == buff.Nanoline && buff.RemainingTime / buff.TotalTime > 0.1)
                    return false;

                if (DynelManager.LocalPlayer.RemainingNCU < Math.Abs(spell.NCU - buff.NCU))
                    return false;
            }
            else
            {
                if (DynelManager.LocalPlayer.RemainingNCU < spell.NCU)
                    return false;
            }

            actionTarget.ShouldSetTarget = true;
            return true;
        }


        private bool EvasivePerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.HealthPercent <= 50)
                return true;

            return false;
        }

        protected bool PetSpawner(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("SpawnPets"))
                return false;

            if (Time.NormalTime < _lastZonedTime + PostZonePetCheckBuffer)
                return false;

            //Do not attempt any pet spawns if we have a pet not loaded as it could be the pet we think we need to replace.
            if (DynelManager.LocalPlayer.Pets.Any(x => x.Type == PetType.Unknown))
                return false;

            if (!RelevantNanos.Pets.ContainsKey(spell.Identity.Instance))
                return false;

            //Ignore spell if we already have this type of pet out
            if (DynelManager.LocalPlayer.Pets.Any(x => x.Type == RelevantNanos.Pets[spell.Identity.Instance].PetType))
                return false;

            //Ignore spell if we already have the shell in our inventory
            if (Inventory.Find(RelevantNanos.Pets[spell.Identity.Instance].ShellId, out Item shell))
                return false;

            actionTarget.ShouldSetTarget = false;
            return true;
        }

        protected virtual bool PetSpawnerItem(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("SpawnPets"))
                return false;

            if (Time.NormalTime < _lastZonedTime + PostZonePetCheckBuffer)
                return false;

            if (!RelevantNanos.Pets.Values.Any(x => (x.ShellId == item.LowId || x.ShellId == item.HighId) && !DynelManager.LocalPlayer.Pets.Any(p => p.Type == x.PetType)))
                return false;

            actionTarget.ShouldSetTarget = false;
            return true;
        }

        protected bool PetTargetBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("BuffPets"))
                return false;

            if (Time.NormalTime < _lastZonedTime + PostZonePetCheckBuffer)
                return false;

            bool petsNeedBuff = false;

            foreach (Pet pet in DynelManager.LocalPlayer.Pets.Where(x => x.Character != null && (x.Type == PetType.Attack || x.Type == PetType.Support)))
            {
                if (pet.Character.Buffs.Find(spell.Nanoline, out Buff buff))
                {
                    //Don't cast if weaker than existing
                    if (spell.StackingOrder < buff.StackingOrder)
                        continue;

                    //Don't cast if greater than 10% time remaining
                    if (spell.Nanoline == buff.Nanoline && buff.RemainingTime / buff.TotalTime > 0.1)
                        continue;
                }

                actionTarget.Target = pet.Character;
                petsNeedBuff = true;
                break;
            }

            if (!petsNeedBuff)
                return false;

            actionTarget.ShouldSetTarget = true;
            return true;
        }

        protected bool PetCorporateStategy(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("BuffPets"))
                return false;

            if (!DynelManager.LocalPlayer.Pets.Where(x => x.Character != null)
                                            .Where(x => x.Type == PetType.Attack)
                                            .Any(x => !x.Character.Buffs.Find(spell.Identity.Instance == 267641 ? 267611 : 285695, out _)))
                return false;

            actionTarget.ShouldSetTarget = false;
            return true;
        }

        protected bool PetInsurancePolicy(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("BuffPets"))
                return false;

            if (!DynelManager.LocalPlayer.Pets.Where(x => x.Character != null)
                                            .Where(x => x.Type == PetType.Attack)
                                            .Any(x => !x.Character.Buffs.Find(spell.Identity.Instance == 267635 ? 267605 : 267605, out _)))
                return false;

            actionTarget.ShouldSetTarget = false;
            return true;
        }

        protected bool PetDroidMatrix(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("BuffPets"))
                return false;

            if (!DynelManager.LocalPlayer.Pets.Where(x => x.Character != null)
                                            .Where(x => x.Type == PetType.Attack)
                                            .Any(x => !x.Character.Buffs.Find(spell.Identity.Instance == 267919 ? 267916 : 285696, out _)))
                return false;

            actionTarget.ShouldSetTarget = false;
            return true;
        }

        protected bool CastTakeTheBullet(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("KillPet"))
                return false;

            if (DynelManager.LocalPlayer.HealthPercent <= 80)

                foreach (Pet pet in DynelManager.LocalPlayer.Pets.Where(x => x.Character != null && (x.Type == PetType.Attack)))
                {
                    actionTarget.Target = pet.Character;
                    return true;
                }

            actionTarget.ShouldSetTarget = false;
            return false;
        }

        private bool CratDebuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            // Check if we are fighting and if debuffing is enabled
            if (fightingTarget == null || !_menu.GetBool("UseDebuff"))
                return false;

            //Check the remaining time on debuffs. On the enemy target
            foreach (Buff buff in fightingTarget.Buffs.AsEnumerable())
            { 
                //Chat.WriteLine(buff.Name);
                if (buff.Name == spell.Name && buff.RemainingTime > 1)
                    return false;
            }

            return true;
        }

        private bool CratNuke(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("UseNukes") || !DynelManager.LocalPlayer.IsAttacking || fightingTarget == null || DynelManager.LocalPlayer.Nano < spell.Cost)
                return false;

            return true;
        }

        protected virtual bool StarfallPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (PerkAction.Find(PerkHash.Combust, out PerkAction combust) && !combust.IsAvailable)
                return false;

            return TargetedDamagePerk(perkAction, fightingTarget, ref actionTarget);
        }

        private bool SingleTargetNuke(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget == null)
                return false;

            return true;
        }

        protected override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);

            CancelBuffs(!_menu.GetBool("UseDemSpeech") ? RelevantNanos.DemSpeechDeadMan : RelevantNanos.CancelBuffsXYZ);

        }

        private static class RelevantNanos
        {
            public const int CompositeAttribute = 223372;
            public const int CompositeNano = 223380;
            public const int CompositeUtility = 287046;
            public const int CompositeRanged = 223348;
            public const int CompositeRangedSpec = 223364;
            public const int ICutRedTape = 222695;
            public const int IHeroicMeasures = 270783;
            public static readonly int[] DemSpeechDeadMan = { 275826, 275826 };
            public static readonly int[] CancelBuffsXYZ = { 275838, 275838 };
            public const int CorporateStrategy = 267611;
            public const int GreaterCorporateInsurancePolicy = 267605;
            public const int DroidDamageMatrix = 267916;
            public const int TakeTheBullet = 267917;

            public const int PinkSlip = 273307;
            public const int WorkplaceDepression = 273631;
            public const int MalaiseOfZeal = 275824;
            public const int IntensifyStress = 222687;
            public const int WastefulArmMovements = 302150;

            public const int LeadershipBuff = 225575;
            public const int GovernanceBuff = 225576;
            public const int TheDirectorBuff = 225574;

            public static Dictionary<int, PetSpellData> Pets = new Dictionary<int, PetSpellData>
            {
                { 258580, new PetSpellData(258580, PetType.Support) }, //Carlo Pinnetti
                { 273300, new PetSpellData(273301, PetType.Attack) }  //CEO Guardian
            };

            


        }

        private class PetSpellData
        {
            public int ShellId;
            public PetType PetType;

            public PetSpellData(int shellId, PetType petType)
            {
                ShellId = shellId;
                PetType = petType;
            }
        }

    }
}
