﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI.Options;
using CombatHandler.Generic;
using System;
using System.Linq;

namespace Desu
{
    public class SoldCombathandler : GenericCombatHandler
    {
        private Menu _menu;
        public SoldCombathandler()
        {
            //LE Procs
            RegisterPerkProcessor(PerkHash.LEProcSoldierFuriousAmmunition, LEProcPerks);   //Type 1
            RegisterPerkProcessor(PerkHash.LEProcSoldierGrazeJugularVein, LEProcPerks);    //Type 2

            // Personal & Group Perks
            RegisterPerkProcessor(PerkHash.FieldBandage, HealSelf);
            RegisterPerkProcessor(PerkHash.Violence, SaveSelf, CombatActionPriority.High);
            RegisterPerkProcessor(PerkHash.Guardian, SaveSelf, CombatActionPriority.High);

            RegisterPerkProcessor(PerkHash.BlessingOfLife, TeamHealPerk);
            RegisterPerkProcessor(PerkHash.Lifeblood, SaveSelf);
            RegisterPerkProcessor(PerkHash.DrawBlood, SaveSelf);

            RegisterPerkProcessor(PerkHash.SupressiveHorde, DmgBuffPerk);
            RegisterPerkProcessor(PerkHash.Clipfever, DamagePerk);
            RegisterPerkProcessor(PerkHash.MuzzleOverload, DamagePerk);

            RegisterPerkProcessor(PerkHash.Energize, DmgBuffPerk);
            RegisterPerkProcessor(PerkHash.PowerVolley, DamagePerk);
            RegisterPerkProcessor(PerkHash.PowerShock, DamagePerk);
            RegisterPerkProcessor(PerkHash.PowerBlast, DamagePerk);
            RegisterPerkProcessor(PerkHash.PowerCombo, DamagePerk);

            RegisterPerkProcessor(PerkHash.ArmorPiercingShot, DamagePerk);
            RegisterPerkProcessor(PerkHash.FindTheFlaw, DamagePerk);
            RegisterPerkProcessor(PerkHash.CalledShot, DamagePerk);

            RegisterPerkProcessor(PerkHash.LegShot, DamagePerk);
            RegisterPerkProcessor(PerkHash.EasyShot, DamagePerk);
            RegisterPerkProcessor(PerkHash.PointBlank, DamagePerk);

            RegisterPerkProcessor(PerkHash.ReinforceSlugs, DmgBuffPerk);
            RegisterPerkProcessor(PerkHash.JarringBurst, DamagePerk);
            RegisterPerkProcessor(PerkHash.SolidSlug, DamagePerk);
            RegisterPerkProcessor(PerkHash.NeutroniumSlug, DamagePerk);

            RegisterPerkProcessor(PerkHash.LaserPaintTarget, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.WeaponBash, DamagePerk);
            RegisterPerkProcessor(PerkHash.TriangulateTarget, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.NapalmSpray, DamagePerk);

            RegisterPerkProcessor(PerkHash.Tracer, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.ContainedBurst, DamagePerk);

            RegisterPerkProcessor(PerkHash.EncaseInStone, DamagePerk);
            RegisterPerkProcessor(PerkHash.DetonateStoneworks, DamagePerk);

            RegisterPerkProcessor(PerkHash.Fuzz, DamagePerk);
            RegisterPerkProcessor(PerkHash.FireFrenzy, DamagePerk);
            RegisterPerkProcessor(PerkHash.BotConfinement, DamagePerk);
            RegisterPerkProcessor(PerkHash.NanoFeast, DamagePerk);

            //Spells
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TotalMirrorShield).OrderByStackingOrder(), AugmentedMirrorShieldMKV);
            RegisterSpellProcessor(RelevantNanos.DontFearTheReaper, DontFearTheReaper);
            RegisterSpellProcessor(RelevantNanos.Distinctvictim, SingleTargetTaunt); //TODO: Generate soldier taunt line to support lower ql taunt use

            //Buffs
            RegisterSpellProcessor(RelevantNanos.CompositeAttribute, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeNano, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeRanged, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeRangedSpec, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.PreNullitySphere, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.FullAutomaticTarg, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.ArtOfWar, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.RiotControl, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.GazumpFight, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.ISemiSentientAugCloud, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.OffensiveSteamroller, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.IPrecognition, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.ISoldierClipJunkie, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.ITotalFocus, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.TotalCombatSurvival, GenericBuff);

            //Items
            RegisterItemProcessor(RelevantItems.DreadlochEnduranceBoosterNanomageEdition, RelevantItems.DreadlochEnduranceBoosterNanomageEdition, EnduranceBooster, CombatActionPriority.High);

            _menu = new Menu("CombatHandler.Sold", "CombatHandler.Sold");
            _menu.AddItem(new MenuBool("useTaunt", "Use Taunt", true));
            _menu.AddItem(new MenuBool("LEProcPerks", "Use LE Procs", true));
            OptionPanel.AddMenu(_menu);
        }

        protected bool LEProcPerks(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("LEProcPerks"))
                return false;

            foreach (Buff buff in DynelManager.LocalPlayer.Buffs.AsEnumerable())
            {
                if (buff.Name == perk.Name)
                {
                    return false;
                }
            }
            return true;
        }

        private bool HealSelf(PerkAction perkAction, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            return DynelManager.LocalPlayer.MissingHealth > 2000;
        }

        private bool SaveSelf(PerkAction perkAction, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            // don't use if we're above 40%
            if (DynelManager.LocalPlayer.HealthPercent > 40)
                return false;

            return true;
        }

        private bool TeamHealPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {

            if (!DynelManager.LocalPlayer.IsAttacking)
                return false;

            // Prioritize keeping ourself alive
            if (DynelManager.LocalPlayer.HealthPercent <= 60)
            {
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            // Try to keep our teammates alive if we're in a team
            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => c.IsAlive)
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.HealthPercent <= 60)
                    .OrderByDescending(c => c.GetStat(Stat.NumFightingOpponents))
                    .FirstOrDefault();

                if (dyingTeamMember != null)
                {
                    actionTarget.Target = dyingTeamMember;
                    return true;
                }
            }
            return false;
        }

        private bool EnduranceBooster(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            // don't use if skill is locked (we will add this dynamically later)
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Strength))
                return false;

            // don't use if we're above 40%
            if (DynelManager.LocalPlayer.HealthPercent > 40)
                return false;

            // don't use if nothing is fighting us
            //if (DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) == 0)
            //    return false;

            // don't use if we have another major absorb running
            // we could check remaining absorb stat to be slightly more effective
            if (DynelManager.LocalPlayer.Buffs.Contains(NanoLine.BioCocoon))
                return false;

            return true;
        }

        private bool DmgBuffPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!DynelManager.LocalPlayer.IsAttacking || fightingTarget == null)
                return false;
            return true;
        }

        private bool SingleTargetTaunt(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("useTaunt") || !DynelManager.LocalPlayer.IsAttacking || fightingTarget == null || DynelManager.LocalPlayer.Nano < spell.Cost)
                return false;

            return true;
        }

        private bool DontFearTheReaper(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            //if (!DynelManager.LocalPlayer.IsAttacking || fightingtarget == null)
            //    return false;

            if (DynelManager.LocalPlayer.HealthPercent <= 40)
                return true;

            return false;
        }

        private bool AugmentedMirrorShieldMKV(Spell spell, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            if (!DynelManager.LocalPlayer.IsAttacking || fightingtarget == null)
                return false;

            if (DynelManager.LocalPlayer.HealthPercent <= 50 && spell.IsReady)
                return true;

            return false;
        }

        private static class RelevantNanos
        {
            public const int CompositeAttribute = 223372;
            public const int CompositeNano = 223380;
            public const int CompositeRanged = 223348;
            public const int CompositeRangedSpec = 223364;

            public const int DontFearTheReaper = 29241;
            public const int Distinctvictim = 223205;
            //public const int AugmentedMirrorShield = 273400; //AMS MK5 - Nanodeck 215 version
            public const int PreNullitySphere = 233033;
            public const int FullAutomaticTarg = 270248;
            public const int ArtOfWar = 275027;
            public const int RiotControl = 29251;
            public const int GazumpFight = 223199;
            public const int ISemiSentientAugCloud = 222838;
            public const int OffensiveSteamroller = 29240;
            public const int IPrecognition = 275844;
            public const int ISoldierClipJunkie = 273402;
            public const int ITotalFocus = 270806;
            public const int TotalCombatSurvival = 273398;
        }

        private static class RelevantItems
        {
            public const int DreadlochEnduranceBooster = 267168;
            public const int DreadlochEnduranceBoosterNanomageEdition = 267167;
        }
    }
}
