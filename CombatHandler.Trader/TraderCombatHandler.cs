﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI.Options;
using CombatHandler.Generic;
using System.Collections.Generic;
using System.Linq;
using AOSharp.Core.UI;
using System;

namespace Desu
{
    public class TraderCombatHandler : GenericCombatHandler
    {
        private Menu _menu;
        public TraderCombatHandler()
        {
            //LE Procs
            RegisterPerkProcessor(PerkHash.LEProcTraderDebtCollection, LEProcPerks);   //Type 1
            RegisterPerkProcessor(PerkHash.LEProcTraderRigidLiquidation, LEProcPerks); //Type 2

            //Personal & Group Perks
            RegisterPerkProcessor(PerkHash.ReapLife, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.Bloodletting, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.VitalShock, TargetedDamagePerk);

            RegisterPerkProcessor(PerkHash.Sacrifice, TargetedDamagePerk);

            RegisterPerkProcessor(PerkHash.LegShot, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.EasyShot, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.PointBlank, TargetedDamagePerk);

            RegisterPerkProcessor(PerkHash.TaintWounds, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.ChemicalBlindness, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.PoisonSprinkle, TargetedDamagePerk);

            RegisterPerkProcessor(PerkHash.Guesstimate, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.MemoryScrabble, TargetedDamagePerk);

            RegisterPerkProcessor(PerkHash.SabotageQuarkField, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.IgnitionFlare, TargetedDamagePerk);

            RegisterPerkProcessor(PerkHash.Energize, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.PowerShock, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.PowerBlast, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.PowerVolley, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.PowerCombo, TargetedDamagePerk);

            RegisterPerkProcessor(PerkHash.NanoFeast, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.BotConfinement, TargetedDamagePerk);

            RegisterPerkProcessor(PerkHash.TapVitae, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.Sacrifice, TargetedDamagePerk);


            //Self Buffs
            RegisterSpellProcessor(RelevantNanos.ImprovedQuantumUncertanity, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.UnstoppableKiller, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.UmbralWranglerPremium, GenericBuff);

            //Team Buffs
            RegisterSpellProcessor(RelevantNanos.QuantumUncertanity, GenericBuff);

            RegisterSpellProcessor(RelevantNanos.ImprovedQuantumUncertanity, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.UnstoppableKiller, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.UmbralWranglerPremium, GenericBuff);

            //Team Nano heal (Rouse Outfit nanoline)
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.NanoPointHeals).OrderByStackingOrder(), TeamNanoHeal);

            //GTH/Your Enemy Drains
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.NanoDrain_LineB), GrandTheftHumidity);
            RegisterSpellProcessor(RelevantNanos.MyEnemiesEnemyIsMyFriend, MyEnemy);

            //AAO/AAD/Damage Drains
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderAADDrain).OrderByStackingOrder(), AADDrain);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderAAODrain).OrderByStackingOrder(), AAODrain);
            RegisterSpellProcessor(RelevantNanos.DivestDamage, DamageDrain);

            //Deprive/Ransack Drains
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderSkillTransferTargetDebuff_Deprive), DepriveDrain);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderSkillTransferTargetDebuff_Ransack), RansackDrain);

            //AC Drains/Debuffs
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderDebuffACNanos).OrderByStackingOrder(), TraderACDrain);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderACTransferTargetDebuff_Draw).OrderByStackingOrder(), TraderACDrain);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.TraderACTransferTargetDebuff_Siphon).OrderByStackingOrder(), TraderACDrain);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.DebuffNanoACHeavy).OrderByStackingOrder(), TraderACDrain);
        

            _menu = new Menu("CombatHandler.Trader", "CombatHandler.Trader");
            //_menu.AddItem(new MenuBool("UseACDrainsDebuffs", "Use AC debuff/drain nano lines", false));
            //_menu.AddItem(new MenuBool("UseLEDrains", "Use AAO'/AAD drain nano lines", false));
            _menu.AddItem(new MenuBool("UseAADDrain", "Use AAD Drains", true)); 
            _menu.AddItem(new MenuBool("UseAAODrain", "Use AAO Drains", true));
            _menu.AddItem(new MenuBool("UseACDrains", "Use AC Drains", true));
            _menu.AddItem(new MenuBool("UseDamageDrain", "Use Damage Drains", true));
            _menu.AddItem(new MenuBool("UseDepriveDrain", "Use Deprive Drains", true));
            _menu.AddItem(new MenuBool("UseRansackDrain", "Use Ransack Drains", true));
            _menu.AddItem(new MenuBool("UseGTH", "Use Grand Theft Humidity", true));
            _menu.AddItem(new MenuBool("UseMyEnemy", "Use My Enemy is My Friend", true));
            _menu.AddItem(new MenuBool("LEProcPerks", "Use LE Procs", true));
            OptionPanel.AddMenu(_menu);
        }

        private static class RelevantNanos
        {
            public const int QuantumUncertanity = 30745;
            public const int ImprovedQuantumUncertanity = 270808;
            public const int UnstoppableKiller = 275846;
            public const int DivestDamage = 273407;
            public const int UmbralWranglerPremium = 235291;
            public const int MyEnemiesEnemyIsMyFriend = 270714;
            public static Dictionary<NanoLine, NanoLine> DebuffToDrainLine = new Dictionary<NanoLine, NanoLine>()
            {
                {NanoLine.TraderAADDrain, NanoLine.TraderNanoTheft2},
                {NanoLine.TraderAAODrain, NanoLine.TraderNanoTheft1},
                {NanoLine.NanoDrain_LineB, NanoLine.NanoOverTime_LineB}
            };
        }

        protected bool LEProcPerks(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("LEProcPerks"))
                return false;

            foreach (Buff buff in DynelManager.LocalPlayer.Buffs.AsEnumerable())
            {
                if (buff.Name == perk.Name)
                {
                    return false;
                }
            }
            return true;
        }

        private bool MyEnemy(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("UseMyEnemy") || fightingTarget == null || fightingTarget.FightingTarget == DynelManager.LocalPlayer)
            {
                return false;
            }

            return true;
        }

        private bool GrandTheftHumidity(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool("UseGTH") || fightingTarget == null)
            {
                return false;
            }

            return true;
        }

        private bool RansackDrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!ToggledDebuff("UseRansackDrain", spell, NanoLine.TraderSkillTransferTargetDebuff_Ransack, fightingTarget, ref actionTarget))
            {
                return false;
            }

            if (DynelManager.LocalPlayer.Buffs.Find(NanoLine.TraderSkillTransferCasterBuff_Ransack, out Buff buff))
            {
                if (buff.RemainingTime > 5)
                {
                    return false;
                }
            }

            return true;
        }

        private bool DepriveDrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!ToggledDebuff("UseDepriveDrain", spell, NanoLine.TraderSkillTransferTargetDebuff_Deprive, fightingTarget, ref actionTarget))
            {
                return false;
            }

            if (DynelManager.LocalPlayer.Buffs.Find(NanoLine.TraderSkillTransferCasterBuff_Deprive, out Buff buff))
            {
                if (buff.RemainingTime > 5)
                {
                    return false;
                }
            }
            return true;
        }

        private bool DamageDrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return ToggledDebuff("UseDamageDrain", spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool AAODrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return ToggledDebuff("UseAAODrain", spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool AADDrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return ToggledDebuff("UseAADDrain", spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool TeamNanoHeal(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            foreach (Buff buff in DynelManager.LocalPlayer.Buffs)
            {
                if (buff.Nanoline == NanoLine.NanoPointHeals)
                {
                    return false;
                }
            }

            // Cast when any team mate is lower than 30% of nano
            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar lowNanoTeamMember = DynelManager.Characters
                    .Where(c => c.Identity != DynelManager.LocalPlayer.Identity) //Do net perk on self
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.Identity != DynelManager.LocalPlayer.Identity)
                    .Where(c => c.NanoPercent <= 30)
                    .OrderByDescending(c => c.GetStat(Stat.NumFightingOpponents))
                    .FirstOrDefault();

                if (lowNanoTeamMember != null)
                {
                    actionTarget.Target = lowNanoTeamMember;
                    return true;
                }
            }

            return false;
        }

        private bool TraderACDrain(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return ToggledDebuff("UseACDrains", spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        private bool ToggledDebuff(string settingName, Spell spell, NanoLine spellNanoLine, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_menu.GetBool(settingName) || fightingTarget == null)
            {
                return false;
            }

            return !fightingTarget.Buffs
                .Where(buff => buff.Nanoline == spellNanoLine) //Same nanoline as the spell nanoline
                .Where(buff => buff.RemainingTime > 1) //Remaining time on buff > 1 second
                .Any(); ;
        }
    }
}
